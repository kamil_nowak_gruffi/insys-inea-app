# README #

### Project set up steps: ###

* cd to project main directory
* npm install
* gulp
* ctrl (cmd) + C
* gulp watch

### Important ###

* This project needs current EPG data in order to work properly. Please download the json file used on http://onlinetv.inea.pl/ and save it as channels.json in public/data/

### Additional notes: ###

* Vertical scrolling on mousewheel on Firefox doesn't work, event isn't propagated from iframe to parent