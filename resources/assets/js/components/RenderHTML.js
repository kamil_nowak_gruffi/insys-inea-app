export default class RenderHtml {
    constructor() {
        RenderHtml.basicHtml();
    }

    static basicHtml(){
        let basicTree = `<div id="epgloader"></div>
                    <div id="message" class="message"></div>
                    <div class="time timeLine">
                        <div class="timeLine-inner"></div>
                    </div>
                    <div class="grid-holder">
                        <div id="channels">
                                <ul></ul>
                        </div>
                        <div class="iframe-holder">
                            <iframe src='grid.html' id='iframe' scrolling='yes' frameborder='0'></iframe>
                        </div>
                    </div>
                    <span id="textTool" style="visiblity:hidden; position: fixed; top: -999px;"></span>
                    <div id="paging" class="pager" style="display: none"></div>'`;
        let epgContainer = $('#epg');
        epgContainer.html(basicTree);
    }

}