export default class Scroller {

    constructor(){
        this.$iframe = $('#iframe');
        this.$gridHolder = $('.grid-holder');
        this.$timeLine = $('.timeLine');
        this.scrollLeft = $(this.$iframe[0].contentWindow).scrollLeft();
        this.scrollTop = this.$iframe.contents().find('html,body').scrollTop();
        this.handleScrolls();
        this.event = new Event('Scrolled');
    }

    /*
     *  1. Listens for mousewheel event on the iframe to handle vertical scrolling
     *  2. Sets a 10ms interval that handles top and left position of the grid and triggers event to load new programs
     */
    handleScrolls(){
        let $iframe = this.$iframe;
        let iframeWindow = $($iframe[0].contentWindow);
        let $gridHolder = this.$gridHolder;

        //FIXME Firefox not propagating mosewheel event from iframe to parent
        iframeWindow.on('mousewheel', function (e) {
            let currentTop = $gridHolder.scrollTop();
            let scrollValue = currentTop - e.originalEvent.wheelDeltaY;
            $gridHolder.scrollTop(scrollValue);
            $iframe.contents().find('html,body').scrollTop(scrollValue);
        });

        setInterval(()=>{
            let currentLeft = $(this.$iframe[0].contentWindow).scrollLeft();
            if(this.scrollLeft != currentLeft){
                this.scrollLeft = currentLeft;
                this.$timeLine.scrollLeft(currentLeft);
                document.dispatchEvent(this.event);
            }
            let currentTop = this.$gridHolder.scrollTop();
            if(this.scrollTop != currentTop){
                this.scrollTop = currentTop;
                this.$iframe.contents().find('html,body').scrollTop(currentTop);
                document.dispatchEvent(this.event);
            }
        }, 10);
    }

    /*
     *  Sets initial horizontal center point when the page loads ( center on current time )
     */
    setCenter(point){
        let $timeLine = this.$timeLine;
        let $iframe = this.$iframe;
        $iframe.contents().find('html,body').scrollLeft(point);
        $timeLine.scrollLeft(point);
    }



}