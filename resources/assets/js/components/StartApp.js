import Scroller from './Scroller.js';

export default class StartApp {
    constructor(selector) {
        this.selector = selector;
        this.iframeContent = '';
        this.iframe = '';
        this.EpgRendered = {};
        this.ChannelsCodnames = '';
        this.ChannelsData = '';
        this.EpgRenderedFuture = [];
    }

    /*
     * Download data from the web server
     */
    getData() {
        const url = $(this.selector).data("url");
        $.ajax({
            method: 'GET',
            url: url,
            dataType: 'json'
        }).done(function (data) {
            StartApp.init(data);
        }.bind(this)).fail(function () {
            console.log('error', url);
        });
    }

    /*
     * Initialize app
     */
    static init(response) {
        const {Channels, AvaibleDaysRaw, PaginationByChannels} = response;

        /*
         * Set iframe to the global value
         */
        this.getIframe();

        /*
         * Render channels with program views
         */
        this.renderChannels(Channels);

        /*
         * Render hour list
         */
        this.renderHours();

        /*
         * Set grid height
         */
        this.setGridSize();

        /*
         * Initiate Scroller object and save it to Scroller variable in this object for later use
         */
        this.Scroller = new Scroller();

        this.Scroller.setCenter(this.getViewPosition()[0]);

        /*
         * Listen to the event, if scrolled add new data to the grid
         */
        document.addEventListener('Scrolled', function(e){
            StartApp.renderEPG(this.ChannelsData);
        }.bind(this));

        // Resizing iframe when window size changed
        window.onresize = ()=>{
            StartApp.setGridSize();
            StartApp.renderEPG(this.ChannelsData);
        };
    }

    static getIframe() {
        this.iframe = $('#iframe');
        this.iframeContent = this.iframe.contents();
    }

    static setGridSize(){
        const gridHldr = $('.grid-holder');
        let height = window.innerHeight - gridHldr.offset().top;
        gridHldr.height(height);
        this.iframe.height(height);
    }

    static renderChannels(Channels) {
        /*
         * list initialize
         */
        let channelList = ``;
        let channelsEpgList = [];
        let channelsCodenameList = [];
        let channelIframeTemplate = ``;
        for (let i = 0, size = Object.keys(Channels).length; i < size; i++) {
            /*
             * get channel properties
             */
            let {ChannelName, ChannelCodename, ChannelImageUrl, Days} = Channels[i];
            /*
             * append channel to list
             */
            ChannelImageUrl += '?m=resize&amp;w=60&amp;h=60';
            channelList += `<li class='chspan ${ChannelCodename}' data-codename='${ChannelCodename}' style="height: 70px;">
                            <span class='image'>
                                <img src='${ChannelImageUrl}' alt='${ChannelCodename}' title='${ChannelName}'>
                            </span>
                            </li>`;
            channelIframeTemplate += `<li id='${ChannelCodename}' class="chspan ${ChannelCodename}" style="width: 9600px; height: 70px;"></li>`;
            /*
             * append available days with epgLists
             */
            channelsEpgList[i] = Days;
            channelsCodenameList.push(ChannelCodename);

        }
        /*
         * append channel list to html
         */
        $('#channels').find('ul').html(channelList.trim());
        /*
         * append channels list to iframe
         */
        $(this.iframeContent).find("#iframeUL").append(channelIframeTemplate.trim());
        /*
         * create EPGList
         */
        this.ChannelsCodnames = channelsCodenameList;
        this.EpgRendered = {};
        this.EpgRenderedFuture = [];
        for (let i = 0, size = channelsCodenameList.length; i < size; i++) {
            this.EpgRendered[channelsCodenameList[i]] = [];
        }

        StartApp.getEpgList(channelsEpgList);
    }

    static getCurrentDate() {
        /*
         * check current date & time
         */
        let date = new Date();
        const currentTime = date.getTime();
        /*
         * Set hours, minutes, seconds to 0 to get proper Day time in ms
         */
        date.setHours(0, 0, 0, 0);
        const currentDay = date.getTime();
        return [currentTime, currentDay];
    }

    static getEpgList(listOfChannels) {
        /*
         * Get today's date
         */
        const currentDay = StartApp.getCurrentDate()[1];
        /*
         * get current day index, by sending first channel and checking available days in data
         */
        const dayIndex = StartApp.getDayIndex(listOfChannels[0], currentDay);
        /*
         * get EPGlist for current date for every channel
         */
        let channelEpgList = [];
        if (dayIndex != -1) {
            for (let i = 0, size = listOfChannels.length; i < size; i++) {

                let {EpgList} = listOfChannels[i][dayIndex];
                channelEpgList[i] = EpgList;

            }
        } else {
            console.log('dayIndex error', dayIndex, currentDay);
        }
        /*
         * send EPGlist to rendering function
         */
        this.ChannelsData = channelEpgList;
        StartApp.renderEPG(channelEpgList);
    }

    /*
     * Returning current Day index, if no data founded returning -1
     */
    static getDayIndex(channelDays, currentDay) {

        for (let i = 0, size = channelDays.length; i < size; i++) {

            if (channelDays[i].Date == currentDay) {
                return i;
            }
        }
        return -1;
    }

    /*
     * Checking current viewport size
     */
    static getViewport() {
        let windowWith, iframeView, windowHeight;
        windowWith = window.innerWidth;
        iframeView = $('#epg').offset().top;
        windowHeight = window.innerHeight - iframeView;

        return [windowWith, windowHeight];
    }

    /*
     * Return list of channels in current view 
     */
    static getChannelsInView() {
        let channelsInView = [];
        const channels = $('#channels').find('ul li');
        let loopStop = false;
        for (let [index, li] of Array.from(channels).entries()) {
            if (StartApp.isOnScreen(li)) {
                loopStop = true;
                channelsInView.push(index);
            } else if (loopStop) {
                break;
            }
        }
        return channelsInView;
    }
    
    /*
     * Render Epg programs
     */
    static renderEPG(channelEpg) {
        let channelsInView = StartApp.getChannelsInView();
        const date = StartApp.getCurrentDate()[1];
        let windowWith = window.innerWidth;
        let begin = Object.keys(this.EpgRendered).length > 0  ? $(this.iframe[0].contentWindow).scrollLeft() : StartApp.getViewPosition()[0];
        let end = begin + windowWith;
        const beginInMs = date + begin * 9000;
        const endInMs = date + end * 9000;
        
        let boxSize;
        let boxPosition;

        /*
         * Loop through selected channels,
         * get programs in view and append to template HTML
         * After second loop append channel programs to DOM
         */
        for (let j = 0, length = Object.keys(channelsInView).length; j < length; j++) {
            let ChannelCodename = this.ChannelsCodnames[channelsInView[j]];
            let channelTemplateHtml = '';
            let renderedChannels = [];

            for (let i = 0, size = Object.keys(channelEpg[channelsInView[j]]).length; i < size; i++) {
                let {Id, Start, Stop, Title} = channelEpg[channelsInView[j]][i];
                let {CategoryDisplayName} = channelEpg[channelsInView[j]][i];
                CategoryDisplayName = CategoryDisplayName === undefined ? '' : CategoryDisplayName;

                if (Start <= endInMs && Stop >= beginInMs && this.EpgRendered[ChannelCodename].findIndex(x => x === Id ) === -1) {
                    renderedChannels.push(Id);
                    boxPosition = StartApp.calculateBoxPosition(date, Start);
                    boxSize = StartApp.calculateBoxSize(Start, Stop);

                    if (boxSize + boxPosition > 9600){
                        boxSize = 9600 - boxPosition;
                    }

                    channelTemplateHtml += `
                    <span id="${Id}" class='program' style="width: ${boxSize}px; left: ${boxPosition}px;" start-time='${Start}' end-time='${Stop}'>
                        <span class="timeProgress">
                        </span>
                        <span class="description">
                            <p class="title"> ${Title} </p>
                            <p> ${CategoryDisplayName} </p>
                        </span>
                        <span class="tooltip bottom in">
                            <span class="tooltip-inner">
                                ${Title} <br> ${CategoryDisplayName}
                            </span>
                        </span>
                    </span>`;
                }
            }
            $(this.iframeContent).find('#'+ChannelCodename).append(channelTemplateHtml.trim());
            this.EpgRenderedFuture = this.EpgRenderedFuture.concat(renderedChannels);
            this.EpgRendered[ChannelCodename] = this.EpgRendered[ChannelCodename].concat(renderedChannels);
        }
        StartApp.programStatus();
    }

    /*
     * Converting epg time to proper box width in px 
     */
    static calculateBoxSize(Start, Stop) {
        return Math.round(((Stop - Start) / 9000) * 100) / 100;
    }


    static calculateBoxPosition(Initial, Actual){
        return Math.round((Actual - Initial) / 9000);
    }


    /*
     * Checking begin, center and end of viewport
     */
    static getViewPosition() {
        let [ width ] = StartApp.getViewport();
        let halfOfWidth = width / 2;
        let [currentTime, currentDay]  = StartApp.getCurrentDate();
        let center = Math.round((currentTime - currentDay) / 9000);
        let begin = center - halfOfWidth;
        let end = center + halfOfWidth;
        return [begin, center, end];
    }

    /*
     * Checking if element is visible - very simple probably that code need's some update.
     */
    static isOnScreen(element) {
        let bounds = element.getBoundingClientRect();
        return bounds.top < window.innerHeight && bounds.bottom > 0;
    }

    /*
     * Render time list boxes - Peter's code
     */
    static renderHours() {
        let hourList = ``;

        for (let i = 0; i < 24; i++) {
            hourList += `<span style="width:200px">${i}:00</span>`;
            hourList += `<span style="width:200px">${i}:30</span>`;
            if (i == 23) {
                hourList += `<span style="width:200px">24:00</span>`;
            }
        }

        $('.timeLine-inner').html(hourList);
        StartApp.timeLinePositionInterval();
    }

    static timeLinePositionInterval(){
        StartApp.timeLinePosition();
        setInterval(()=>{StartApp.timeLinePosition();}, 9000);
    }


    static timeLinePosition() {
        StartApp.programStatus();
        $(this.iframeContent).find(".timeLine").css('display: block');
        $(this.iframeContent).find("#tline").css('left', StartApp.getViewPosition()[1] + 'px');
    }

    static programStatus(){
        let tLinePosition = StartApp.getViewPosition()[1];

        for (let index = Object.keys(this.EpgRenderedFuture).length-1; index >= 0; index--) {
            let program =  $(this.iframeContent).find(`#${this.EpgRenderedFuture[index]}`);
            let start = program.attr('start-time');
            let end = program.attr('end-time');
            let currentDate = StartApp.getCurrentDate()[1];

            /*
             * Have to calculate box position instead reading css properties from DOM,
             * because in Firefox we get left = 0 <- probably, because element wasn't
             * full rendered when we get element from DOM
             */
            let width = StartApp.calculateBoxSize(start,end);
            let left = StartApp.calculateBoxPosition(currentDate, start);
            let totalWidth = width + left;

            if (left > tLinePosition){
                continue;
            }

            // Add past class to elements before timeLine
            if (left < tLinePosition && tLinePosition >= totalWidth){
                if (program.hasClass('current')){
                    program.removeClass('current');
                }
                program.addClass('past');
                this.EpgRenderedFuture.splice(index, 1);
            } else if (tLinePosition <= totalWidth) {
                if (!program.hasClass('current')){
                    program.addClass('current').children('.timeProgress').append('<span class="bar"></span>');
                    program.find('.bar').css('width', (tLinePosition - left) + 'px');
                } else{
                    program.find('.bar').css('width', (tLinePosition - left) + 'px');
                }
            }
        }
    }
}
