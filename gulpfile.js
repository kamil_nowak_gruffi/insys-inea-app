var elixir = require('laravel-elixir');
var gulp = require('gulp');
require('wo-laravel-elixir-jade');
require('elixir-jshint');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    elixir.config.js.browserify.watchify = {
        enabled: true,
        options: {
            poll: true
        }
    };
    /*
     * BrowserSync setup
     */
    mix.browserSync({
        proxy: false,
        server: {
            baseDir: './public'
        }
    });

    /*
     * Front assets
     */
    mix.sass('app.scss');

    mix.scripts([
        '../bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    ], 'public/js/vendor.js');

    mix.jshint(['resources/assets/js/**/*.js'], {
        esversion: 6
    });

    mix.browserify('app.js');


    mix.jade('**/*.jade', 'public', {extension: '.html'});

    /*
     * Copy required fonts and images
     */
    //mix.copy('', '');


});

